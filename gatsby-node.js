"use strict";

var DrupalMarkdownNodes = ["node__markdown"];
exports.onCreateNode = function (_ref) {
  var node = _ref.node,
    createNodeId = _ref.createNodeId,
    createContentDigest = _ref.createContentDigest,
    actions = _ref.actions;
  var createNode = actions.createNode,
    createParentChildLink = actions.createParentChildLink;
  var thisType = node.internal.type === "node__markdown" ? "Markdown" : null;
  if (thisType) {
    // generate MarkdownRemark and PaneFragment
    var markdownNode = {
      id: createNodeId(node.id + " MarkdownRemark"),
      parent: node.id,
      children: [],
      internal: {
        type: thisType,
        mediaType: "text/markdown",
        content: node === null || node === void 0 ? void 0 : node.field_markdown_body
      }
    };
    markdownNode.frontmatter = {
      title: node.field_alt_description || node.title,
      id: node.id
    };
    markdownNode.internal.contentDigest = createContentDigest(markdownNode);
    createNode(markdownNode);
    createParentChildLink({
      parent: node,
      child: markdownNode
    });
    return markdownNode;
  }
  return {};
};
exports.createSchemaCustomization = function (_ref2) {
  var actions = _ref2.actions;
  //actions.printTypeDefinitions({ path: "./typeDefs.txt" })
  var createTypes = actions.createTypes;
  var typeDefs = "\n  type node__markdown implements Node {\n    title: String\n    field_slug: String\n    field_category_slug: String\n    field_markdown_body: String\n    field_alt_description: String\n    field_is_context_pane: Boolean\n    field_image: [node__markdownField_image]\n    field_image_svg: [node__markdownField_image_svg]\n  }\n  type node__markdownRelationships {\n    field_image: [file__file] @link(by: \"id\", from: \"field_image___NODE\")\n    field_image_svg: [file__file] @link(by: \"id\", from: \"field_image_svg___NODE\")\n    node__pane: [node__pane] @link(by: \"id\", from: \"node__pane___NODE\")\n  }\n  type node__markdownField_image {\n    description: String\n    drupal_internal__target_id: Int\n  }\n  type node__markdownField_image_svg {\n    description: String\n    drupal_internal__target_id: Int\n  }\n  type node__story_fragment implements Node {\n    title: String\n    field_slug: String\n    field_social_image_path: String\n    field_tailwind_background_colour: String\n    relationships: node__story_fragmentRelationships\n    field_menu: node__story_fragmentField_menu\n    field_panes: [node__story_fragmentField_panes]\n    field_context_panes: [node__story_fragmentField_context_panes]\n    field_tract_stack: node__story_fragmentField_tract_stack\n  }\n  type node__story_fragmentField_menu {\n    drupal_internal__target_id: Int\n  }\n  type node__story_fragmentField_panes {\n    drupal_internal__target_id: Int\n  }\n  type node__tractstackField_context_panes {\n    drupal_internal__target_id: Int\n  }\n  type node__story_fragmentField_context_panes {\n    drupal_internal__target_id: Int\n  }\n  type node__story_fragmentField_tract_stack {\n    drupal_internal__target_id: Int\n  }\n\n  type node__story_fragmentRelationships {\n    field_menu: node__menu @link(by: \"id\", from: \"field_menu___NODE\")\n    field_panes: [node__pane] @link(by: \"id\", from: \"field_panes___NODE\")\n    field_context_panes: [node__pane] @link(by: \"id\", from: \"field_context_panes___NODE\")\n    field_tract_stack: node__tractstack @link(by: \"id\", from: \"field_tract_stack___NODE\")\n    node__tractstack: [node__tractstack] @link(by: \"id\", from: \"node__tractstack___NODE\")\n  }\n\ntype node__resource implements Node {\n  status: Boolean\n  title: String\n  field_action_lisp: String\n  field_oneliner: String\n  field_options: String\n  field_slug: String\n  field_category_slug: String\n}\n\n  type node__tractstack implements Node {\n    field_slug: String\n    field_social_image_path: String\n    relationships: node__tractstackRelationships\n    field_story_fragments: [node__tractstackField_story_fragments]\n    field_context_panes: [node__tractstackField_context_panes]\n  }\n\n  type node__pane implements Node {\n    title: String\n    field_height_offset_desktop: Int\n    field_height_offset_mobile: Int\n    field_height_offset_tablet: Int\n    field_height_ratio_desktop: String\n    field_height_ratio_mobile: String\n    field_height_ratio_tablet: String\n    field_slug: String\n    field_options: String\n    relationships: node__paneRelationships\n    field_markdown: [node__markdown]\n    field_image: [node__paneField_image]\n    field_image_svg: [node__paneField_image_svg]\n  }\n  type node__paneRelationships {\n    node__story_fragment: [node__story_fragment] @link(by: \"id\", from: \"node__story_fragment___NODE\")\n    node__markdown: [node__markdown] @link(by: \"id\", from: \"node__markdown___NODE\")\n    field_image: [file__file] @link(by: \"id\", from: \"field_image___NODE\")\n    field_image_svg: [file__file] @link(by: \"id\", from: \"field_image_svg___NODE\")\n  }\n  type node__paneField_image {\n    description: String\n    drupal_internal__target_id: Int\n  }\n  type node__paneField_image_svg {\n    description: String\n    drupal_internal__target_id: Int\n  }\n\n  type node__menu implements Node {\n    field_image_logo: node__menuField_image_logo\n    field_svg_logo: node__menuField_svg_logo\n    field_options: String\n    field_pixel_height_desktop: Int\n    field_pixel_height_mobile: Int\n    field_pixel_height_tablet: Int\n    field_theme: String\n    relationships: node__menuRelationships\n }\n  type node__menuRelationships {\n    field_image_logo: file__file @link(by: \"id\", from: \"field_image_logo___NODE\")\n    field_svg_logo: file__file @link(by: \"id\", from: \"field_svg_logo___NODE\")\n    node__story_fragment: [node__story_fragment] @link(by: \"id\", from: \"node__story_fragment___NODE\")\n  }\n  type node__menuField_image_logo {\n    display: Boolean\n    description: String\n    drupal_internal__target_id: Int\n  }\n  type node__menuField_svg_logo {\n    display: Boolean\n    description: String\n    drupal_internal__target_id: Int\n  }\n\n  type node__tractstackRelationships {\n    field_story_fragments: [node__story_fragment] @link(by: \"id\", from: \"field_story_fragments___NODE\")\n    field_context_panes: [node__pane] @link(by: \"id\", from: \"field_context_panes___NODE\")\n    node__story_fragment: [node__story_fragment] @link(by: \"id\", from: \"node__story_fragment___NODE\")\n  }\n \n  type node__tractstackField_story_fragments {\n    drupal_internal__target_id: Int\n  }\n\n  type file__file implements Node {\n    relationships: file__fileRelationships\n    localFile: File @link(by: \"id\", from: \"localFile___NODE\")\n  }\n\n  type file__fileRelationships {\n    node__menu: [node__menu] @link(by: \"id\", from: \"node__menu___NODE\")\n    node__markdown: [node__markdown] @link(by: \"id\", from: \"node__markdown___NODE\")\n  }\n\n  ";
  createTypes(typeDefs);
};

/*
exports.createResolvers = ({ createResolvers }) => {
  const resolvers = {
    PaneFragment: {
      childMarkdownRemark: {
        type: ["PaneFragment"],
        resolve: async (source, args, context, info) => {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                htmlAst: { eq: true },
              },
            },
            type: "PaneFragment",
          });
          return entries;
        },
      },
    },
  };
  createResolvers(resolvers);
};
*/
//# sourceMappingURL=gatsby-node.js.map