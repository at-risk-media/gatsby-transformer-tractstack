const DrupalMarkdownNodes = ["node__markdown"]

exports.onCreateNode = ({
  node,
  createNodeId,
  createContentDigest,
  actions,
}) => {
  const { createNode, createParentChildLink } = actions
  const thisType = node.internal.type === `node__markdown` ? `Markdown` : null
  if (thisType) {
    // generate MarkdownRemark and PaneFragment
    const markdownNode = {
      id: createNodeId(`${node.id} MarkdownRemark`),
      parent: node.id,
      children: [],
      internal: {
        type: thisType,
        mediaType: `text/markdown`,
        content: node?.field_markdown_body,
      },
    }
    markdownNode.frontmatter = {
      title: node.field_alt_description || node.title,
      id: node.id,
    }
    markdownNode.internal.contentDigest = createContentDigest(markdownNode)
    createNode(markdownNode)
    createParentChildLink({ parent: node, child: markdownNode })
    return markdownNode
  }
  return {}
}

exports.createSchemaCustomization = ({ actions }) => {
  //actions.printTypeDefinitions({ path: "./typeDefs.txt" })
  const { createTypes } = actions
  const typeDefs = `
  type node__markdown implements Node {
    title: String
    field_slug: String
    field_category_slug: String
    field_markdown_body: String
    field_alt_description: String
    field_is_context_pane: Boolean
    field_image: [node__markdownField_image]
    field_image_svg: [node__markdownField_image_svg]
  }
  type node__markdownRelationships {
    field_image: [file__file] @link(by: "id", from: "field_image___NODE")
    field_image_svg: [file__file] @link(by: "id", from: "field_image_svg___NODE")
    node__pane: [node__pane] @link(by: "id", from: "node__pane___NODE")
  }
  type node__markdownField_image {
    description: String
    drupal_internal__target_id: Int
  }
  type node__markdownField_image_svg {
    description: String
    drupal_internal__target_id: Int
  }
  type node__story_fragment implements Node {
    title: String
    field_slug: String
    field_social_image_path: String
    field_tailwind_background_colour: String
    relationships: node__story_fragmentRelationships
    field_menu: node__story_fragmentField_menu
    field_panes: [node__story_fragmentField_panes]
    field_context_panes: [node__story_fragmentField_context_panes]
    field_tract_stack: node__story_fragmentField_tract_stack
  }
  type node__story_fragmentField_menu {
    drupal_internal__target_id: Int
  }
  type node__story_fragmentField_panes {
    drupal_internal__target_id: Int
  }
  type node__tractstackField_context_panes {
    drupal_internal__target_id: Int
  }
  type node__story_fragmentField_context_panes {
    drupal_internal__target_id: Int
  }
  type node__story_fragmentField_tract_stack {
    drupal_internal__target_id: Int
  }

  type node__story_fragmentRelationships {
    field_menu: node__menu @link(by: "id", from: "field_menu___NODE")
    field_panes: [node__pane] @link(by: "id", from: "field_panes___NODE")
    field_context_panes: [node__pane] @link(by: "id", from: "field_context_panes___NODE")
    field_tract_stack: node__tractstack @link(by: "id", from: "field_tract_stack___NODE")
    node__tractstack: [node__tractstack] @link(by: "id", from: "node__tractstack___NODE")
  }

type node__resource implements Node {
  status: Boolean
  title: String
  field_action_lisp: String
  field_oneliner: String
  field_options: String
  field_slug: String
  field_category_slug: String
}

  type node__tractstack implements Node {
    field_slug: String
    field_social_image_path: String
    relationships: node__tractstackRelationships
    field_story_fragments: [node__tractstackField_story_fragments]
    field_context_panes: [node__tractstackField_context_panes]
  }

  type node__pane implements Node {
    title: String
    field_height_offset_desktop: Int
    field_height_offset_mobile: Int
    field_height_offset_tablet: Int
    field_height_ratio_desktop: String
    field_height_ratio_mobile: String
    field_height_ratio_tablet: String
    field_slug: String
    field_options: String
    relationships: node__paneRelationships
    field_markdown: [node__markdown]
    field_image: [node__paneField_image]
    field_image_svg: [node__paneField_image_svg]
  }
  type node__paneRelationships {
    node__story_fragment: [node__story_fragment] @link(by: "id", from: "node__story_fragment___NODE")
    node__markdown: [node__markdown] @link(by: "id", from: "node__markdown___NODE")
    field_image: [file__file] @link(by: "id", from: "field_image___NODE")
    field_image_svg: [file__file] @link(by: "id", from: "field_image_svg___NODE")
  }
  type node__paneField_image {
    description: String
    drupal_internal__target_id: Int
  }
  type node__paneField_image_svg {
    description: String
    drupal_internal__target_id: Int
  }

  type node__menu implements Node {
    field_image_logo: node__menuField_image_logo
    field_svg_logo: node__menuField_svg_logo
    field_options: String
    field_pixel_height_desktop: Int
    field_pixel_height_mobile: Int
    field_pixel_height_tablet: Int
    field_theme: String
    relationships: node__menuRelationships
 }
  type node__menuRelationships {
    field_image_logo: file__file @link(by: "id", from: "field_image_logo___NODE")
    field_svg_logo: file__file @link(by: "id", from: "field_svg_logo___NODE")
    node__story_fragment: [node__story_fragment] @link(by: "id", from: "node__story_fragment___NODE")
  }
  type node__menuField_image_logo {
    display: Boolean
    description: String
    drupal_internal__target_id: Int
  }
  type node__menuField_svg_logo {
    display: Boolean
    description: String
    drupal_internal__target_id: Int
  }

  type node__tractstackRelationships {
    field_story_fragments: [node__story_fragment] @link(by: "id", from: "field_story_fragments___NODE")
    field_context_panes: [node__pane] @link(by: "id", from: "field_context_panes___NODE")
    node__story_fragment: [node__story_fragment] @link(by: "id", from: "node__story_fragment___NODE")
  }
 
  type node__tractstackField_story_fragments {
    drupal_internal__target_id: Int
  }

  type file__file implements Node {
    relationships: file__fileRelationships
    localFile: File @link(by: "id", from: "localFile___NODE")
  }

  type file__fileRelationships {
    node__menu: [node__menu] @link(by: "id", from: "node__menu___NODE")
    node__markdown: [node__markdown] @link(by: "id", from: "node__markdown___NODE")
  }

  `
  createTypes(typeDefs)
}

/*
exports.createResolvers = ({ createResolvers }) => {
  const resolvers = {
    PaneFragment: {
      childMarkdownRemark: {
        type: ["PaneFragment"],
        resolve: async (source, args, context, info) => {
          const { entries } = await context.nodeModel.findAll({
            query: {
              filter: {
                htmlAst: { eq: true },
              },
            },
            type: "PaneFragment",
          });
          return entries;
        },
      },
    },
  };
  createResolvers(resolvers);
};
*/
